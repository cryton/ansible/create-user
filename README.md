# Ansible role - Create user
This role creates a new user and installs ACL to allow vagrant to log in.

## Supported platforms

| Platform | version   |
|----------|-----------|
| Debian   | \>=11     |
| Kali     | \>=2022.1 |

## Requirements

- Root access (specify `become` directive as a global or while invoking the role)
    ```yaml
    become: yes
    ```

## Parameters
The parameters and their defaults can be found in the `defaults/main.yml` file.

It is recommended to update the `user_password` variable.

| variable      | description           |
|---------------|-----------------------|
| run_as_user   | Which user to create. |
| user_password | What password to use. |

## Examples

### Usage
```yaml
  roles:
    - role: create-user
      become: yes

```

### Inclusion

[https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies)
```yaml
- name: create-user
  src: https://gitlab.ics.muni.cz/cryton/ansible/create-user.git
  version: "master"
  scm: git
```
